﻿// AltEx.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <float.h>
#include <iostream>
#include <string>
#include "gnuplot_i.h"

#define STRING_SIZE 256

typedef struct point
{
	double x;
	double y;
	double z;
}Point;

typedef struct flat
{
	int A;
	int B;
	int C;
	double D;
}Flat;

typedef struct straight
{
	Flat f0;
	Flat f1;
	short int needed;
}Straight;


typedef struct vector {
	int A;
	int B;
	int C;
} Vector;

typedef struct pointNStraight
{
	Point P;
	Straight S;
}PointNStraight;



typedef struct monomial {
	int coeX;
	int coeY;
	double coeFree;
} Monomial;

typedef struct polyhedron
{
	PointNStraight* points = NULL;
	int size;
} Polyhedron;

typedef struct plotPoints
{
	Polyhedron *polyhedrons = NULL;
	int size;
} PlotPoints;

typedef struct edges
{
	Flat Z_bottom, Z_top, X_bottom, X_top, Y_bottom, Y_top;
} Edges;

typedef struct tLinearEquation {
	monomial *part = NULL;
	int length;
} tLinearEquation;

void clean_stdin();
std::string setGnuplotPathFromFile();
void writeGnuplotPathToFile();//
tLinearEquation formTLE(std::string input);//Перевод многочлена-строки из тропического вида в набор коэффициентов
void printTLE(tLinearEquation thisTLE, std::string semiring);//Вывод многочлена
void buildGraphDat(plotPoints plotPoints);//Построение графика по массиву точек
std::string shorten(std::string str);//Обрезание строки с числом с плавающей запятой до двух символов после запятой
Vector straightToVector(Straight s);
double length(Point p1, Point p2);
double det(int A, int B, double D, int A1, int B1, double D1, int A2, int B2, double C2);
Vector normal(Flat flat);
Vector projection(Vector normal, Flat flat);
double angleVectors(Vector v1, Vector v2);
Straight intersectionTwoFlats(Flat f0, Flat f1);
Straight* intersectionOneWithAllFlats(Flat f0, Flat* f, int cnt, int *uniqFlatsReturn);
Point intersectionTwoStraight(Straight s0, Straight s1, double C);
PointNStraight singleGraphPoint(PointNStraight ps0, Straight* s, int cnt, int coordRange, PointNStraight psStart, int iterationNumber);
PointNStraight startPointNStraight(Flat* m, int cnt, int coordRange, Edges Edges);
Flat* monomialToFlat(Monomial* m, int cnt);
Polyhedron pointsSinglePolyhedron(Straight* s, PointNStraight ps0, int cnt, int coordRange);
int comparePolyhedron(Polyhedron pol1, Polyhedron pol2);
int comparePointNStraights(PointNStraight pns1, PointNStraight pns2);
int comparePoints(Point p1, Point p2);
int compareFlats(Flat f1, Flat f2);
int compareStraights(Straight s1, Straight s2);
PlotPoints makeAllPolyhedron(Flat* f, int cnt, PointNStraight ps0, Edges Edges);
Edges makeEdges(int coordRange);

template<class ForwardIt, class UnaryPredicate>
ForwardIt remove_if(ForwardIt first, ForwardIt last,
	UnaryPredicate p);

int main()
{
	int cont;//Флаг выхода из меню
	char mainMenu;//Меню
	int cntFlats; //размер массива плоскостей

	PointNStraight ps0;
	tLinearEquation polynomial;//Тропический полином
	plotPoints  plotPoints;//набор точек, по которому нужно построить график
	std::string input,//Строка вводимого тропического полинома
		semiring;//Полукольцо
	std::string gnuPlotPath;//Строка для передачи пути к gnuplot

	Flat* flats;
	Flat f0;
	Edges Edges;
	int coordRange;//Граница графика по осям

	clock_t start, end;//Замер времени выполнения функции
	double cpuTimeUsed;

	gnuPlotPath = setGnuplotPathFromFile();
	Gnuplot::set_GNUPlotPath(gnuPlotPath);

	//try { 
	Gnuplot gp;
	//}
	//catch (Gnuplot) {
	//	puts("Error at including gnuplot!");
	//}

	input = "No input yet";
	semiring = "max";
	coordRange = 30;
	Edges = makeEdges(coordRange);

	input = "x+y";
	polynomial = formTLE(input);
	cont = 1;
	do {
		system("CLS");
		puts("3d tropical polynomial visualization\n");
		printf("1 to enter tropical polynomial, current: %s.\n", input.c_str());
		printf("2 to set coordinate range, current: %i.\n", coordRange);
		printf("3 to switch between min and max semiring, current: %s.\n", semiring.c_str());
		puts("4 to plot it");
		puts("0 to quit");
		mainMenu = getchar();
		clean_stdin();
		switch (mainMenu) {
		case '1':
			printf("\nEnter polynomial:\n");
			std::cin >> input;
			clean_stdin();
			polynomial = formTLE(input);
			printTLE(polynomial, semiring);
			printf("\nSuccesfull input\nPress ENTER to continue");
			getchar();
			break;

		case '2':
			printf("\nEnter coordinate range:\n");
			scanf("%d", &coordRange);
			clean_stdin();
			Edges = makeEdges(coordRange);

			printf("\nCoordinate range set to %i\nPress ENTER to continue", coordRange);
			getchar();
			break;

		case '3':

			if (semiring == "max")
				semiring = "min";
			else if (semiring == "min")
				semiring = "max";
			printf("\nSwitched to %s\nPress ENTER to continue", semiring.c_str());
			getchar();
			break;

		case '4':
			puts("\nPlotting...");
			start = clock();

			flats = monomialToFlat(polynomial.part, polynomial.length);
			cntFlats = polynomial.length;
			flats = (Flat*)realloc(flats, (cntFlats + 6) * sizeof(Flat));
			if (flats != NULL)
			{
				flats[cntFlats] = Edges.Z_bottom;
				flats[cntFlats + 1] = Edges.Z_top;
				flats[cntFlats + 2] = Edges.X_bottom;
				flats[cntFlats + 3] = Edges.X_top;
				flats[cntFlats + 4] = Edges.Y_bottom;
				flats[cntFlats + 5] = Edges.Y_top;
				cntFlats = cntFlats + 6;
			}
			else
				puts("Error at memory allocation!");
			ps0 = startPointNStraight(flats, cntFlats, coordRange, Edges);
			plotPoints = makeAllPolyhedron(flats, cntFlats, ps0, Edges);

			buildGraphDat(plotPoints);
			//gp.cmd("set pm3d\n");
			//gp.cmd("splot \"graph.dat\" with pm3d\n");

			gp.cmd("set hidden3d\n");
			gp.cmd("splot \"graph.dat\" with lines\n");

			end = clock();
			cpuTimeUsed = ((double)(end - start)) / CLOCKS_PER_SEC;
			printf("\nPlot is ready\nCPU time: %f s\nPress ENTER to continue", cpuTimeUsed);
			getchar();
			break;

		case '0':
			cont = 0;
			break;

		default:
			puts("Wrong input!");
			break;

		}
	} while (cont != 0);

	std::cout << std::endl << "Press ENTER to continue..." << std::endl;
	std::cin.clear();
	std::cin.ignore(std::cin.rdbuf()->in_avail());
	std::cin.get();

	return 0;
}

Edges makeEdges(int coordRange) {
	Edges Edges;
	Edges.X_bottom.A = 1;
	Edges.X_bottom.B = 0;
	Edges.X_bottom.C = 0;
	Edges.X_bottom.D = -coordRange;
	Edges.Y_bottom.A = 0;
	Edges.Y_bottom.B = 1;
	Edges.Y_bottom.C = 0;
	Edges.Y_bottom.D = -coordRange;
	Edges.Z_bottom.A = 0;
	Edges.Z_bottom.B = 0;
	Edges.Z_bottom.C = 1;
	Edges.Z_bottom.D = -coordRange;

	Edges.X_top.A = 1;
	Edges.X_top.B = 0;
	Edges.X_top.C = 0;
	Edges.X_top.D = coordRange;
	Edges.Y_top.A = 0;
	Edges.Y_top.B = 1;
	Edges.Y_top.C = 0;
	Edges.Y_top.D = coordRange;
	Edges.Z_top.A = 0;
	Edges.Z_top.B = 0;
	Edges.Z_top.C = 1;
	Edges.Z_top.D = coordRange;


	return Edges;
}

void clean_stdin() {
	while (getchar() != '\n');
}

double det(int A, int B, double D, int A1, int B1, double D1, int A2, int B2, double C2)
{
	return A * B1*C2 + B * D1*A2 + D * A1*B2 - D * B1*A2 - B * A1*C2 - A * D1*B2;
}

Vector normal(Flat flat) {
	Vector normal;

	normal.A = flat.A;
	normal.B = flat.B;
	normal.C = flat.C;

	return normal;
}

Vector projection(Vector normal, Flat flat) {

	Vector projection;

	projection.A = normal.A;
	projection.B = normal.B;
	projection.C = flat.C;

	return projection;
}

double angleVectors(Vector v1, Vector v2) {
	double angle;

	angle = acos((v1.A * v1.B * v1.C + v2.A * v2.B * v2.C) /
		(sqrt(v1.A * v1.A + v1.B * v1.B + v1.C * v1.C)
			* sqrt(v2.A * v2.A + v2.B * v2.B + v2.C * v2.C)));

	return angle;
}

//Передается дискриминант, ибо если он равен нулю, то прямые не пересечкаются, а считать два раза нет смысла.
Point intersectionTwoStraight(Straight s0, Straight s1, double C)
{
	Point p;

	p.x = det(s0.f0.D, s0.f0.B, s0.f0.C, s0.f1.D, s0.f1.B, s0.f1.C, s1.f1.D, s1.f1.B, s1.f1.C) / C;
	p.y = det(s0.f0.A, s0.f0.D, s0.f0.C, s0.f1.A, s0.f1.D, s0.f1.C, s1.f1.A, s1.f1.D, s1.f1.C) / C;
	p.z = det(s0.f0.A, s0.f0.B, s0.f0.D, s0.f1.A, s0.f1.B, s0.f1.D, s1.f1.A, s1.f1.B, s1.f1.D) / C;

	return p;
}

Straight intersectionTwoFlats(Flat f0, Flat f1)
{
	Straight s;

	s.f0 = f0; s.f1 = f1;

	return s;
}

Straight* intersectionOneWithAllFlats(Flat f0, Flat* f, int cnt, int *uniqFlatsReturn)
{
	Straight* s;
	int i, j, uniqFlats;

	uniqFlats = 0;
	for (int e = 0; e < cnt; e++)
		if (compareFlats(f0, f[e]) != 1)
			uniqFlats += 1;

	s = (Straight*)malloc((uniqFlats) * sizeof(Straight));
	if (s != NULL)
	{
		for (i = 0, j = 0; i < cnt; i++)
		{
			if (compareFlats(f0, f[i]) != 1) {
				s[j] = intersectionTwoFlats(f0, f[i]);
				j++;
			}

		}
	}
	else puts("Error of memory!");
	*uniqFlatsReturn = uniqFlats;

	return s;
}
double length(Point p1, Point p2)
{
	double length;

	length = sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y) + (p1.z - p2.z)*(p1.z - p2.z));

	return length;
}

Vector straightToVector(Straight s)
{
	Vector v;
	v.A = s.f1.B*s.f0.C - s.f0.B*s.f1.C;
	v.B = s.f0.A*s.f1.C - s.f1.A*s.f0.C;
	v.C = s.f0.A*s.f1.B - s.f1.A*s.f0.B;

	return v;
}

Flat* monomialToFlat(Monomial* m, int cnt)
{
	Flat* f = NULL;
	int i;

	f = (Flat*)malloc(cnt * sizeof(Flat));
	if (f != NULL)
	{
		for (i = 0; i < cnt; i++)
		{
			f[i].A = m[i].coeX;
			f[i].B = m[i].coeY;
			f[i].C = -1;
			f[i].D = -m[i].coeFree;
		}
	}
	else puts("Error at memoy allocation!");

	return f;
}

PointNStraight startPointNStraight(Flat* m, int cnt, int coordRange, Edges Edges)
{
	PointNStraight ps0;
	Flat m0;
	Flat f0;
	double z1, z2, ang1, ang2;
	int i;
	int F1 = 0;
	int F2 = 0;

	f0.A = 0;
	f0.B = 0;
	f0.C = 1;
	f0.D = 0;

	for (i = 0; i < (cnt - 5); i++)
	{
		if (i == 0)
		{
			z1 = m[i].A*(-coordRange) + m[i].B*(-coordRange) + m[i].D;
			m0.A = m[i].A;
			m0.B = m[i].B;
			m0.C = m[i].C; //m0.C = -1
			m0.D = m[i].D;

		}
		else
		{
			z2 = m[i].A*(-coordRange) + m[i].B*(-coordRange) + m[i].D;
			if (z2 >= z1)
			{
				if (z2 == z1) 
				{
					if ((angleVectors(projection(normal(m0), f0), projection(normal(m[i]), f0)) == 0)
						&& (angleVectors(projection(normal(m0), f0), normal(m0)) > angleVectors(projection(normal(m[i]), f0), normal(m[i]))
							|| (angleVectors(straightToVector(intersectionTwoFlats(m[i], Edges.X_bottom)), straightToVector(intersectionTwoFlats(f0, Edges.X_bottom)))
							> angleVectors(straightToVector(intersectionTwoFlats(m0, Edges.X_bottom)), straightToVector(intersectionTwoFlats(f0, Edges.X_bottom)))))) {
						m0.A = m[i].A;
						m0.B = m[i].B;
						m0.C = m[i].C; //m0.C = -1
						m0.D = m[i].D;
					}
					if (angleVectors(straightToVector(intersectionTwoFlats(m[i], Edges.X_bottom)), straightToVector(intersectionTwoFlats(f0, Edges.X_bottom)))
						== angleVectors(straightToVector(intersectionTwoFlats(m0, Edges.X_bottom)), straightToVector(intersectionTwoFlats(f0, Edges.X_bottom)))
						&& angleVectors(straightToVector(intersectionTwoFlats(m[i], Edges.Y_bottom)), straightToVector(intersectionTwoFlats(f0, Edges.Y_bottom)))
						> angleVectors(straightToVector(intersectionTwoFlats(m0, Edges.Y_bottom)), straightToVector(intersectionTwoFlats(f0, Edges.Y_bottom))))
					{
						m0.A = m[i].A;
						m0.B = m[i].B;
						m0.C = m[i].C; //m0.C = -1
						m0.D = m[i].D;
						F1 = 1;
					}
				}
				else
				{
					z1 = z2;
					m0.A = m[i].A;
					m0.B = m[i].B;
					m0.C = m[i].C; //m0.C = -1
					m0.D = m[i].D;
				}
			}
		}

	}
	ps0.P.x = -coordRange;
	ps0.P.y = -coordRange;
	ps0.P.z = z1;
	ps0.S.f0 = m0;
	if (ps0.S.f0.A != 0 || F1 == 1) // Если плоскость пересекает грань X_bottom
		ps0.S.f1 = Edges.X_bottom;
	else// Иначе, если плоскость пересекает грань Y_bottom
		ps0.S.f1 = Edges.Y_bottom;

	return ps0;
}

PointNStraight singleGraphPoint(PointNStraight ps1, Straight* s, int cnt, int coordRange, PointNStraight psStart, int iterationNumber)
{
	PointNStraight G;
	Point p;
	int i, j, quitFunction;
	double C, l1, l2;
	Vector v0, v1, v2;

	i = 0, j = 0; quitFunction = 0; l1 = DBL_MAX;

	for (i = 0; (i < cnt) && (quitFunction == 0); i++)
	{
		if (s[i].needed != 0) 
		{
			C = det(ps1.S.f0.A, ps1.S.f0.B, ps1.S.f0.C, ps1.S.f1.A, ps1.S.f1.B, ps1.S.f1.C, s[i].f1.A, s[i].f1.B, s[i].f1.C);
			if (C != 0)
			{
				p = intersectionTwoStraight(ps1.S, s[i], C);
				if ((abs(p.x) <= coordRange) && (abs(p.y) <= coordRange) && (abs(p.z) <= coordRange) && (comparePoints(psStart.P, p) != 1))
				{
					l2 = length(ps1.P, p);
					if (j == 0)
					{
						G.S = s[i];
						G.P = p;
						if (l2 != 0)
							l1 = l2;
						j = 1;
					}
					else 
					{
						if (l2 <= l1 && l2 != 0) 
						{
							v0 = straightToVector(ps1.S);
							v1 = straightToVector(s[i]);
							v2 = straightToVector(G.S);
							if (l2 < l1) 
							{
								l1 = l2;
								G.P = p;
								G.S = s[i];
							}
							else if ((l2 == l1) && (angleVectors(v0, v1) <= angleVectors(v0, v2))) 
							{
								G.S = s[i];
							}
						}
					}
				} 
				if ((comparePoints(p, psStart.P) == 1) && (iterationNumber > 0))//Добавил обработку наличия пути в одно ребро 19.5.31 1:00
				{
					G.P = p;
					G.S = s[i];
					quitFunction = 1;
				}
			}
			if ((C == 0) && (compareStraights(s[i], psStart.S) == 1) && (iterationNumber > 0) /*&& (comparePoints( ps1.P, psStart.P) == 1)*/)
				G = psStart;
		}
	}

	return G;
}

Polyhedron pointsSinglePolyhedron(Straight* s, PointNStraight ps0, int cnt, int coordRange)
{
	Polyhedron pol;
	PointNStraight ps1, psStart, temp;
	int i, key, j, flag, flag2, funcQuit;

	i = 0; key = 0; flag2 = 0; funcQuit = 0;
	psStart = ps0;
	//if (abs(psStart.P.z) > coordRange)
	//{
	//	if ((psStart.S.f0.A > 1) && (psStart.S.f0.B == 0))
	//	{
	//		psStart.P.x = (psStart.S.f0.D - (psStart.S.f0.B + psStart.S.f0.C) * (-coordRange)) / (psStart.S.f0.A);
	//		psStart.P.y = -coordRange;//для верхнего полукольца
	//		psStart.P.z = -coordRange;
	//		psStart.S.f1.A = 0;
	//		psStart.S.f1.B = 1;
	//	}
	//	if ((psStart.S.f0.B > 1) && (psStart.S.f0.A == 0))
	//	{
	//		psStart.P.y = (psStart.S.f0.D - (psStart.S.f0.A + psStart.S.f0.C) * (-coordRange)) / (psStart.S.f0.B);
	//		psStart.P.x = -coordRange;//для верхнего полукольца
	//		psStart.P.z = -coordRange;
	//		psStart.S.f1.A = 1;
	//		psStart.S.f1.B = 0;
	//	}
	//}
	ps1 = psStart;
	do
	{
		flag = 1;
		temp = singleGraphPoint(ps1, s, cnt, coordRange, psStart, i);
		
		if (flag2 == 1)
		{
			if (comparePoints(temp.P, pol.points[i - 1].P) == 1)
			{
				flag = 0;
			}
			if (compareStraights(temp.S, psStart.S) == 1)
				key = 1;
			if (comparePoints(temp.P, psStart.P) == 1)//Почему мы не проверяем точку? 19.5.31 1:00
				key = 1;
		}

		if (flag != 0)
		{
			pol.points = (PointNStraight*)realloc(pol.points, (i + 1) * sizeof(PointNStraight));
			if (pol.points != NULL)
			{
				flag2 = 1;
				pol.points[i] = temp;
				for (j = 0; j < cnt; j++)
					if ((compareStraights(s[j], pol.points[i].S) == 1) && (compareStraights(s[j], psStart.S) != 1))
						s[j].needed = 0;//Отбрасываем прямые уже использованные как грани многоугольника
				i++;
			}
			else puts("Error at memory allocation!\n");
			/*			}*/
		}
		ps1 = temp;
	} while (key == 0);
	pol.size = i;

	return pol;
}

//функция принимает массив всех плоскостей, вклюяающий стороны куба, и его размер,
//начальный PointNStraight и некоторые стороны куба ввиде плоскостей 
PlotPoints makeAllPolyhedron(Flat* f, int cnt, PointNStraight ps0, Edges Edges)
{
	int i, j, q, key, uniqFlats;
	Straight* s;
	PlotPoints ALL;
	Polyhedron poss;

	i = 0; j = 0; q = 0; key = 0;
	ALL.polyhedrons = (Polyhedron*)malloc(sizeof(Polyhedron));
	if (ALL.polyhedrons != NULL)
	{
		s = intersectionOneWithAllFlats(ps0.S.f0, f, cnt, &uniqFlats);
		ALL.polyhedrons[i] = pointsSinglePolyhedron(s, ps0, uniqFlats, Edges.X_top.D);
		ALL.size = 1;
	}
	else puts("Error of memory!!");

	for (i = 0; i < ALL.size; i++)//проходимся по всем многогранникам
	{
		for (j = 0; j < ALL.polyhedrons[i].size; j++)//проходим по всем PoiuntNStraight данного многогранника
		{//Здесь проверка на то, что ALL.p.S.f0 не является гранью куба, если является, то для него не строим многогранник
			if ((compareFlats(ALL.polyhedrons[i].points[j].S.f1, Edges.Y_bottom) != 1) && (compareFlats(ALL.polyhedrons[i].points[j].S.f1, Edges.X_bottom) != 1)
				&& (compareFlats(ALL.polyhedrons[i].points[j].S.f1, Edges.Z_bottom) != 1) && (compareFlats(ALL.polyhedrons[i].points[j].S.f1, Edges.Y_top) != 1)
				&& (compareFlats(ALL.polyhedrons[i].points[j].S.f1, Edges.Z_top) != 1) && (compareFlats(ALL.polyhedrons[i].points[j].S.f1, Edges.X_top) != 1))
			{
				//строим многогранник
				s = intersectionOneWithAllFlats(ALL.polyhedrons[i].points[j].S.f1, f, cnt, &uniqFlats);
				poss = pointsSinglePolyhedron(s, ALL.polyhedrons[i].points[j], uniqFlats, Edges.X_top.D);
				//проверка на уже существующий многоранник 
				for (q = 0; q < ALL.size; q++)
					if (compareFlats(poss.points[0].S.f0, ALL.polyhedrons[q].points[0].S.f0) == 1)
						key = 1;
				if (key == 0)
				{
					//добавляем многогранник в список наших многогранников
					ALL.size++;
					ALL.polyhedrons = (Polyhedron*)realloc(ALL.polyhedrons, (ALL.size) * sizeof(Polyhedron));
					if (ALL.polyhedrons != NULL)
					{
						ALL.polyhedrons[ALL.size - 1] = poss;
					}
					else puts("Error of memory!!");
				}
				key = 0;
			}
		}
	}
	return ALL;
}

int compareStraights(Straight s1, Straight s2) {
	int result;

	if ((compareFlats(s1.f0, s2.f0) == 1) && (compareFlats(s1.f1, s2.f1) == 1))
		result = 1;
	else
		result = 0;

	return result;
}

int compareFlats(Flat f1, Flat f2) {
	int result;

	if ((f1.A == f2.A) && (f1.B == f2.B) && (f1.C == f2.C) && (f1.D == f2.D))
		result = 1;
	else
		result = 0;

	return result;
}

int comparePoints(Point p1, Point p2) {
	int result;

	if ((p1.x == p2.x) && (p1.y == p2.y) && (p1.z == p2.z))
		result = 1;
	else
		result = 0;

	return result;
}

int comparePointNStraights(PointNStraight pns1, PointNStraight pns2) {
	int result;

	if ((comparePoints(pns1.P, pns2.P) == 1) && (compareStraights(pns1.S, pns2.S) == 1))
		result = 1;
	else
		result = 0;

	return result;
}

int comparePolyhedron(Polyhedron pol1, Polyhedron pol2) {
	int result, key, i;

	key = 1;
	if (pol1.size == pol2.size)
	{
		for (i = 0; i < pol1.size; i++)
			if (comparePointNStraights(pol1.points[i], pol2.points[i]) != 1)
				key = 0;
		if (key == 1)
			result = 1;
		else
			result = 0;
	}
	else
		result = 0;

	return result;
}

std::string setGnuplotPathFromFile() {
	FILE *gnuplotPathFile;
	std::string gnuplotPathString;
	char* gnuplotPathStringLegacy;

	gnuplotPathStringLegacy = (char*)malloc(STRING_SIZE * sizeof(char));
	gnuplotPathFile = fopen("gnuplotPath.txt", "r");
	if (gnuplotPathFile) {
		fgets(gnuplotPathStringLegacy, STRING_SIZE, gnuplotPathFile);
	}
	else
		puts("Error at file opening!");
	gnuplotPathString = gnuplotPathStringLegacy;
	free(gnuplotPathStringLegacy);

	return gnuplotPathString;
}

void writeGnuplotPathToFile() {
	FILE *gnuplotPathFile;

	gnuplotPathFile = fopen("gnuplotPath.txt", "w");
	if (gnuplotPathFile) {
		fputs("../gnuplot/bin", gnuplotPathFile);
		puts("Succes!");
	}
	else
		puts("Error at file opening!");
}

tLinearEquation formTLE(std::string input)
{
	tLinearEquation thisTLE;
	int cutX,
		cutY,
		curPlus,
		cut,
		cutStart,
		cutEnd;
	std::string tempString,
		coeString;

	thisTLE.length = 0;
	curPlus = 0;
	input.erase(remove_if(input.begin(), input.end(), isspace), input.end());

	do {
		thisTLE.length++;
		thisTLE.part = (Monomial*)realloc(thisTLE.part, thisTLE.length * sizeof(Monomial));
		curPlus = input.find_first_of("+", 0);
		if (curPlus != std::string::npos) {
			tempString = input.substr(0, curPlus);
			input = input.substr(curPlus + 1, size(input));
		}
		else
			tempString = input;
		tempString.push_back('|');

		cut = tempString.find_first_of("*xy|", 0);//Поиск свободного коэффициента
		if (cut != 0) {
			coeString = tempString.substr(0, cut);
			thisTLE.part[thisTLE.length - 1].coeFree = std::stod(coeString);
		}
		else
			thisTLE.part[thisTLE.length - 1].coeFree = 0;
		if (tempString[cut] == '*')
			tempString = tempString.substr(cut + 1, size(tempString));
		else {
			tempString = tempString.substr(tempString.find_first_of("xy|", 0), size(tempString));
		}

		cut = tempString.find_first_of("x", 0);//Поиск коэффициента при X
		if (cut != std::string::npos) {
			if (tempString[cut + 1] == '^') {
				cutStart = tempString.find_first_of("^", cut) + 1;
				cutEnd = tempString.find_first_of("*y|", cut) - 1;
				coeString = tempString.substr(cutStart, cutEnd - 1);
				thisTLE.part[thisTLE.length - 1].coeX = std::stoi(coeString);
			}
			else
				thisTLE.part[thisTLE.length - 1].coeX = 1;

			cutY = (tempString.find_first_of("y", 0));
			cutX = (tempString.find_first_of("x", 0));
			if (cutY > cutX)
				tempString = tempString.substr(0, cutX - 1);
			else
				if (cutY != std::string::npos)
					tempString = tempString.substr(cutY, size(tempString));
		}
		else
			thisTLE.part[thisTLE.length - 1].coeX = 0;


		cut = tempString.find_first_of("y", 0);//Поиск коэффициента при Y
		if (cut != std::string::npos) {
			if (tempString[cut + 1] == '^') {
				cutStart = tempString.find_first_of("^", cut) + 1;
				cutEnd = tempString.find_first_of("*x|", cut) - 1;
				coeString = tempString.substr(cutStart, cutEnd - 1);
				thisTLE.part[thisTLE.length - 1].coeY = std::stoi(coeString);
			}
			else
				thisTLE.part[thisTLE.length - 1].coeY = 1;
			//if (tempString[cut + 1] == '^')//Удаление части подстроки
			//	tempString = tempString.substr(cutEnd + 1, size(tempString));
			//else
			//	tempString = tempString.substr(cut + 1, size(tempString));
		}
		else
			thisTLE.part[thisTLE.length - 1].coeY = 0;

	} while (curPlus != std::string::npos);

	return thisTLE;
}

void printTLE(tLinearEquation thisTLE, std::string semiring)
{
	printf("%s {", semiring.c_str());
	for (int i = 0; i < thisTLE.length; i++) {
		if (thisTLE.part[i].coeFree != 0)
			printf("%5.2f", thisTLE.part[i].coeFree);
		if (thisTLE.part[i].coeX != 0) {
			if (thisTLE.part[i].coeFree != 0)
				printf(" + ");
			if (thisTLE.part[i].coeX == 1)
				printf("x");
			else
				printf("%ix", thisTLE.part[i].coeX);
		}

		if (thisTLE.part[i].coeY != 0) {
			if ((thisTLE.part[i].coeX != 0) || (thisTLE.part[i].coeFree != 0))
				printf(" + ");
			if (thisTLE.part[i].coeY == 1)
				printf("y");
			else
				printf("%iy", thisTLE.part[i].coeY);
		}
		if (i + 1 < thisTLE.length)
			printf(", ");
	}
	printf("}");
}

void buildGraphDat(plotPoints plotPoints) {
	FILE *graphDat = NULL;
	std::string temp;
	double coordinate;

	graphDat = fopen("graph.dat", "w");
	if (graphDat) {
		temp = "# X    Y    Z\n";
		fprintf(graphDat, temp.c_str());
		for (int i = 0; i < plotPoints.size; i++) {//один из всех многоугольников
			for (int j = 0; j < plotPoints.polyhedrons[i].size; j++) {//одна из точек многоугольника
				temp = shorten(std::to_string(plotPoints.polyhedrons[i].points[j].P.x)) + "  "
					+ shorten(std::to_string(plotPoints.polyhedrons[i].points[j].P.y)) + "  "
					+ shorten(std::to_string(plotPoints.polyhedrons[i].points[j].P.z)) + "\n";
				fprintf(graphDat, temp.c_str());
			}
			temp = shorten(std::to_string(plotPoints.polyhedrons[i].points[0].P.x)) + "  "
				+ shorten(std::to_string(plotPoints.polyhedrons[i].points[0].P.y)) + "  "
				+ shorten(std::to_string(plotPoints.polyhedrons[i].points[0].P.z)) + "\n";
			fprintf(graphDat, temp.c_str());
			fprintf(graphDat, "\n");
		}
		fclose(graphDat);
		puts("Succes!");
	}
	else
		puts("Error at file opening!");
}

std::string shorten(std::string str) {
	str = str.substr(0, str.find_first_of(".", 0) + 3);

	return str;
}

template<class ForwardIt, class UnaryPredicate>
ForwardIt remove_if(ForwardIt first, ForwardIt last,
	UnaryPredicate p)
{
	ForwardIt result = first;
	for (; first != last; ++first) {
		if (!p(*first)) {
			*result++ = *first;
		}
	}
	return result;
}